package com.charuniverse.sipresensi.data.services

import com.charuniverse.sipresensi.data.models.Dosen
import com.charuniverse.sipresensi.data.models.Mahasiswa
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.data.models.Presensi

interface FirestoreServices {
    suspend fun addDosen(dosen: Dosen)
    suspend fun getDosen(): List<Dosen>

    suspend fun addMahasiswa(mahasiswa: Mahasiswa)
    suspend fun getMahasiswa(): List<Mahasiswa>

    suspend fun addMataKuliah(mataKuliah: MataKuliah)
    suspend fun getMatakuliah(): List<MataKuliah>

    suspend fun addPresensi(presensi: Presensi)
    suspend fun getPresensi(): List<Presensi>
    suspend fun getPresensiByNim(nim: String): List<Presensi>
}