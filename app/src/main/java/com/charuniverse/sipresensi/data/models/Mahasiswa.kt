package com.charuniverse.sipresensi.data.models

import com.charuniverse.sipresensi.utils.AppPreferences

data class Mahasiswa(
        val username: String = "",
        val password: String = "",
        val nama: String = AppPreferences.namaMahasiswa,
        val nim: String = AppPreferences.nimMahasiswa,
        val prodi: String = AppPreferences.prodiMahasiswa,
        val email: String = AppPreferences.emailMahasiswa,
        val nik: String = "",
        val nomor_hp: String = ""
)
