package com.charuniverse.sipresensi.data.models

import com.charuniverse.sipresensi.utils.AppPreferences
import com.google.firebase.Timestamp

data class Presensi(
    val nama_matakuliah: String = "",
    val kelas: String = "",
    var status: String = "",
    var link_foto: String? = null,
    val nama_mahasiswa: String = AppPreferences.namaMahasiswa,
    val nim_mahasiswa: String = AppPreferences.nimMahasiswa,
    val tanggal: Timestamp = Timestamp.now()
)
