package com.charuniverse.sipresensi.data.models

data class MataKuliah(
    val nama_matakuliah: String = "",
    val nama_dosen: String = "",
    val kelas: List<String> = listOf()
)
