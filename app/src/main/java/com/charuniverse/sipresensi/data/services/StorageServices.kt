package com.charuniverse.sipresensi.data.services

import android.net.Uri

interface StorageServices {
    suspend fun uploadImageToStorage(imageUri: Uri, storagePath: String): String
}