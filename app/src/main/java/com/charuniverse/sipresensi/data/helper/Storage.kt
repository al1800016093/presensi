package com.charuniverse.sipresensi.data.helper

import android.net.Uri
import com.charuniverse.sipresensi.data.services.StorageServices
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.tasks.await

object Storage : StorageServices {

    private val storageRef: FirebaseStorage by lazy {
        FirebaseStorage.getInstance()
    }


    override suspend fun uploadImageToStorage(imageUri: Uri, storagePath: String): String {
        storageRef.maxUploadRetryTimeMillis = 30_000

        val pathRef = storageRef.reference.child(storagePath)
        pathRef.putFile(imageUri).await()
        return pathRef.downloadUrl.await().toString()
    }

}