package com.charuniverse.sipresensi.data.models

import com.charuniverse.sipresensi.utils.AppPreferences

data class Dosen(
        val username: String = "",
        val password: String = "",
        val nama: String = AppPreferences.namaDosen,
        val nip: String = AppPreferences.nipDosen,
        val email: String = AppPreferences.emailDosen,
)