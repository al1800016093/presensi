package com.charuniverse.sipresensi.data.helper

import com.charuniverse.sipresensi.data.models.Dosen
import com.charuniverse.sipresensi.data.models.Mahasiswa
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.data.models.Presensi
import com.charuniverse.sipresensi.data.services.FirestoreServices
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.tasks.await

object Firestore : FirestoreServices {

    private val firestoreRef: FirebaseFirestore by lazy {
        FirebaseFirestore.getInstance()
    }

    private val dosenRef: CollectionReference by lazy {
        firestoreRef.collection("Dosen")
    }

    private val mahasiswaRef: CollectionReference by lazy {
        firestoreRef.collection("Mahasiswa")
    }

    private val matakuliahRef: CollectionReference by lazy {
        firestoreRef.collection("Matakuliah")
    }

    private val presensiRef: CollectionReference by lazy {
        firestoreRef.collection("Presensi")
    }


    override suspend fun addDosen(dosen: Dosen) {
        dosenRef.add(dosen).await()
    }

    override suspend fun getDosen(): List<Dosen> {
        val docs = dosenRef.get().await()
        return docs.toObjects(Dosen::class.java)
    }

    override suspend fun addMahasiswa(mahasiswa: Mahasiswa) {
        mahasiswaRef.add(mahasiswa).await()
    }

    override suspend fun getMahasiswa(): List<Mahasiswa> {
        val docs = mahasiswaRef.get().await()
        return docs.toObjects(Mahasiswa::class.java)
    }

    override suspend fun addMataKuliah(mataKuliah: MataKuliah) {
        matakuliahRef.document(mataKuliah.nama_matakuliah)
            .set(mataKuliah).await()
    }

    override suspend fun getMatakuliah(): List<MataKuliah> {
        val docs = matakuliahRef.get().await()
        return docs.toObjects(MataKuliah::class.java)
    }

    override suspend fun addPresensi(presensi: Presensi) {
        presensiRef.add(presensi).await()
    }

    override suspend fun getPresensi(): List<Presensi> {
        val docs = presensiRef
            .orderBy("nama_matakuliah", Query.Direction.ASCENDING)
            .orderBy("kelas", Query.Direction.ASCENDING)
            .orderBy("tanggal", Query.Direction.DESCENDING)
            .get().await()
        return docs.toObjects(Presensi::class.java)
    }

    override suspend fun getPresensiByNim(nim: String): List<Presensi> {
        val docs = presensiRef
            .whereEqualTo("nim_mahasiswa", nim)
            .orderBy("nama_matakuliah", Query.Direction.ASCENDING)
            .orderBy("kelas", Query.Direction.ASCENDING)
            .orderBy("tanggal", Query.Direction.DESCENDING)
            .get().await()
        return docs.toObjects(Presensi::class.java)
    }
}