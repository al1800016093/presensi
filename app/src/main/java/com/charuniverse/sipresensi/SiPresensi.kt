package com.charuniverse.sipresensi

import android.app.Application
import com.charuniverse.sipresensi.utils.AppPreferences

class SiPresensi : Application() {
    override fun onCreate() {
        super.onCreate()
        AppPreferences.init(this)
    }
}