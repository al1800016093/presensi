package com.charuniverse.sipresensi.utils

object Constants {
    const val INPUT_ERROR = "Harap isikan semua form yang ada"

    const val NETWORK_ERROR_MESSAGE = "Terjadi masalah pada jaringan"
    const val LOGIN_DOSEN_ERROR_MESSAGE = "Nama atau NIP yang anda masukkan salah"
    const val LOGIN_MAHASISWA_ERROR_MESSAGE = "Usename atau password yang anda masukkan salah"

    const val PRESENSI_PRESENT = "Hadir"
    const val PRESENSI_SICK = "Sakit"

    const val REQUEST_CODE_IMAGE = 120
}