package com.charuniverse.sipresensi.utils

import android.content.Context
import android.content.SharedPreferences
import com.charuniverse.sipresensi.data.models.Dosen
import com.charuniverse.sipresensi.data.models.Mahasiswa

object AppPreferences {

    private const val NAME = "SiPresensi"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    //SharedPreferences variables
    private val prefNamaDosen = Pair("nama_dosen", "")
    private val prefNipDosen = Pair("nik_dosen", "")
    private val prefEmailDosen = Pair("email_dosen", "")

    private val prefNamaMahasiswa = Pair("nama_mhs", "")
    private val prefNimMahasiswa = Pair("nim_mhs", "")
    private val prefProdiMahasiswa = Pair("prodi_mhs", "")
    private val prefEmailMahasiswa = Pair("email_mhs", "")


    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    //an inline function to put variable and save it
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var namaDosen: String
        get() = preferences.getString(prefNamaDosen.first, prefNamaDosen.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefNamaDosen.first, value)
        }

    var nipDosen: String
        get() = preferences.getString(prefNipDosen.first, prefNipDosen.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefNipDosen.first, value)
        }

    var emailDosen: String
        get() = preferences.getString(prefEmailDosen.first, prefEmailDosen.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefEmailDosen.first, value)
        }


    var namaMahasiswa: String
        get() = preferences.getString(prefNamaMahasiswa.first, prefNamaMahasiswa.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefNamaMahasiswa.first, value)
        }

    var nimMahasiswa: String
        get() = preferences.getString(prefNimMahasiswa.first, prefNimMahasiswa.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefNimMahasiswa.first, value)
        }

    var prodiMahasiswa: String
        get() = preferences.getString(prefProdiMahasiswa.first, prefProdiMahasiswa.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefProdiMahasiswa.first, value)
        }

    var emailMahasiswa: String
        get() = preferences.getString(prefEmailMahasiswa.first, prefEmailMahasiswa.second) ?: ""
        set(value) = preferences.edit {
            it.putString(prefEmailMahasiswa.first, value)
        }


    fun hasDataDosen(): Boolean {
        return namaDosen.isNotEmpty() &&
                nipDosen.isNotEmpty() &&
                emailDosen.isNotEmpty()
    }

    fun saveDataDosen(dosen: Dosen) {
        namaDosen = dosen.nama
        nipDosen = dosen.nip
        emailDosen = dosen.email
    }

    fun getDataDosen(): Dosen {
        return Dosen(
            nama = namaDosen,
            nip = nipDosen,
            email = emailDosen
        )
    }

    fun hasDataMahasiswa(): Boolean {
        return namaMahasiswa.isNotEmpty() &&
                nimMahasiswa.isNotEmpty() &&
                prodiMahasiswa.isNotEmpty() &&
                emailMahasiswa.isNotEmpty()
    }

    fun saveDataMahasiswa(mahasiswa: Mahasiswa) {
        namaMahasiswa = mahasiswa.nama
        nimMahasiswa = mahasiswa.nim
        prodiMahasiswa = mahasiswa.prodi
        emailMahasiswa = mahasiswa.email
    }

    fun getDataMahasiswa(): Mahasiswa {
        return Mahasiswa(
            nama = namaMahasiswa,
            nim = nimMahasiswa,
            prodi = prodiMahasiswa,
            email = emailMahasiswa
        )
    }

    fun deleteDataDosen() {
        namaDosen = ""
        nipDosen = ""
        emailDosen = ""
    }

    fun deleteDataMahasiswa() {
        namaMahasiswa = ""
        nimMahasiswa = ""
        prodiMahasiswa = ""
        emailMahasiswa = ""
    }
}