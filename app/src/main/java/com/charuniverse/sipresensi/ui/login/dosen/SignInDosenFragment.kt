package com.charuniverse.sipresensi.ui.login.dosen

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.login.LoginViewModel
import com.charuniverse.sipresensi.utils.Constants
import kotlinx.android.synthetic.main.fragment_sign_in_dosen.*

class SignInDosenFragment : Fragment(R.layout.fragment_sign_in_dosen) {

    private lateinit var viewModel: LoginViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel(view)
        onButtonClicked()
    }

    private fun initViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
            .also {
                it.initializeViewModel(view, requireActivity())
                it.getDataDosen()
            }
    }

    private fun onButtonClicked() {
        ivSignInDosenBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        btnSignUpDosen.setOnClickListener {
            findNavController().navigate(R.id.action_signInDosenFragment_to_signUpDosenFragment)
        }

        cvSignInDosen.setOnClickListener {
            it.isEnabled = false
            processSignIn()
        }
    }

    private fun processSignIn() {
        val name = etSignInDosenName.text.toString()
        val nip = etSignInDosenNip.text.toString()

        if (name.isEmpty() || nip.isEmpty()) {
            Toast.makeText(requireContext(), Constants.INPUT_ERROR, Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.validateLoginDosen(name, nip)
        cvSignInDosen.isEnabled = true
    }

}