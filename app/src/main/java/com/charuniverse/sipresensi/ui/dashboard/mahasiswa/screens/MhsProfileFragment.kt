package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.login.LoginActivity
import com.charuniverse.sipresensi.utils.AppPreferences
import kotlinx.android.synthetic.main.fragment_mhs_profile.*

class MhsProfileFragment : Fragment(R.layout.fragment_mhs_profile) {

    private val prefs = AppPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()

        ivMhsProfileBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        tvMhsLogOut.setOnClickListener {
            prefs.deleteDataMahasiswa()
            
            requireActivity().startActivity(Intent(requireContext(), LoginActivity::class.java))
            requireActivity().finish()
        }
    }

    private fun setupUI() {
        prefs.let {
            tvMhsProfileName.text = it.namaMahasiswa
            tvMhsProfileNim.text = it.nimMahasiswa
            tvMhsProfileProdi.text = it.prodiMahasiswa
            tvMhsProfileEmail.text = it.emailMahasiswa
        }
    }
}