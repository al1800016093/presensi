package com.charuniverse.sipresensi.ui.login.dosen

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.Dosen
import com.charuniverse.sipresensi.ui.login.LoginViewModel
import com.charuniverse.sipresensi.utils.Constants
import kotlinx.android.synthetic.main.fragment_sign_up_dosen.*

class SignUpDosenFragment : Fragment(R.layout.fragment_sign_up_dosen) {

    private lateinit var viewModel: LoginViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel(view)
        onButtonClicked()
    }

    private fun initViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
            .also {
                it.initializeViewModel(view, requireActivity())
            }
    }

    private fun onButtonClicked() {
        ivSignUpDosenBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        cvSignUpDosen.setOnClickListener {
            it.isEnabled = false
            processSignUp()
        }
    }

    private fun processSignUp() {
        val username = etSignUpDosenUsername.text.toString()
        val password = etSignUpDosenPassword.text.toString()
        val name = etSignUpDosenName.text.toString()
        val nip = etSignUpDosenNip.text.toString()
        val email = etSignUpDosenEmail.text.toString()

        if (username.isEmpty() || password.isEmpty() || name.isEmpty() || nip.isEmpty() || email.isEmpty()) {
            Toast.makeText(requireContext(), Constants.INPUT_ERROR, Toast.LENGTH_SHORT)
                .show()
            return
        }

        viewModel.addDosen(Dosen(username, password, name, nip, email))
        cvSignUpDosen.isEnabled = true
    }
}