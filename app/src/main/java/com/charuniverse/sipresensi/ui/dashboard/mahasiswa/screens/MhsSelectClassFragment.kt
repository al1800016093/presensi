package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.ui.adapters.MhsClassAdapter
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import kotlinx.android.synthetic.main.fragment_mhs_select_class.*

class MhsSelectClassFragment : Fragment(R.layout.fragment_mhs_select_class) {

    private lateinit var viewModel: MahasiswaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        listenToViewModel()

        ivMhsSelectClassBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun listenToViewModel() {
        viewModel.currentSubject.observe(viewLifecycleOwner, {
            setupRecyclerUI(it)
            tvSelectClassSubject.text = it.nama_matakuliah
        })
    }

    private fun setupRecyclerUI(item: MataKuliah) {
        recyclerClass.let {
            val sortedClass = item.kelas.sorted()
            it.layoutManager = LinearLayoutManager(requireContext())
            it.adapter = MhsClassAdapter(viewModel, sortedClass)
        }
    }

}