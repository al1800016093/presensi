package com.charuniverse.sipresensi.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenDashboardActivity
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MhsDashboardActivity
import com.charuniverse.sipresensi.utils.AppPreferences

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        AppPreferences.let {
            if (it.hasDataDosen()) {
                startActivity(Intent(this, DosenDashboardActivity::class.java))
                finish()
            } else if (it.hasDataMahasiswa()) {
                startActivity(Intent(this, MhsDashboardActivity::class.java))
                finish()
            }
        }
    }

}