package com.charuniverse.sipresensi.ui.login.mahasiswa

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.Mahasiswa
import com.charuniverse.sipresensi.ui.login.LoginViewModel
import com.charuniverse.sipresensi.utils.Constants
import kotlinx.android.synthetic.main.fragment_mhs_sign_up.*

class MhsSignUpFragment : Fragment(R.layout.fragment_mhs_sign_up) {

    private lateinit var viewModel: LoginViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel(view)
        onButtonClicked()
    }

    private fun initViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
            .also {
                it.initializeViewModel(view, requireActivity())
            }
    }

    private fun onButtonClicked() {
        ivSignUpMhsBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        cvSignUpMhs.setOnClickListener {
            it.isEnabled = false
            processSignUp()
        }
    }

    private fun processSignUp() {
        val username = etSignUpMhsUsername.text.toString()
        val password = etSignUpMhsPassword.text.toString()
        val name = etSignUpMhsNama.text.toString()
        val nim = etSignUpMhsNim.text.toString()
        val prodi = etSignUpMhsProdi.text.toString()
        val email = etSignUpMhsEmail.text.toString()
        val nik = etSignUpMhsNik.text.toString()
        val phone = etSignUpMhsPhone.text.toString()

        if (username.isEmpty() || password.isEmpty() || name.isEmpty() || nim.isEmpty() || prodi.isEmpty() || email.isEmpty() || nik.isEmpty() || phone.isEmpty()) {
            Toast.makeText(requireContext(), Constants.INPUT_ERROR, Toast.LENGTH_SHORT)
                .show()
            return
        }

        viewModel.addMahasiswa(
            Mahasiswa(username, password, name, nim, prodi, email, nik, phone)
        )
        cvSignUpMhs.isEnabled = true
    }
}
