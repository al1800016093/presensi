package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.fragment_dosen_dashboard.*

class DosenDashboardFragment : Fragment(R.layout.fragment_dosen_dashboard) {

    private lateinit var viewModel: DosenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)

        llDosenCheckPresence.setOnClickListener {
            viewModel.navigateToSelectSubject()
        }

        llDosenProfile.setOnClickListener {
            findNavController().navigate(R.id.action_dosenDashboardFragment_to_dosenProfileFragment)
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(DosenViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

}