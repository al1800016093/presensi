package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.login.LoginActivity
import com.charuniverse.sipresensi.utils.AppPreferences
import kotlinx.android.synthetic.main.fragment_dosen_profile.*

class DosenProfileFragment : Fragment(R.layout.fragment_dosen_profile) {

    private val prefs = AppPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivDosenProfileBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        tvDosenLogOut.setOnClickListener {
            prefs.deleteDataDosen()

            requireActivity().let {
                it.startActivity(Intent(requireContext(), LoginActivity::class.java))
                it.finish()
            }
        }

        setupUI()
    }

    private fun setupUI() {
        prefs.let {
            tvDosenProfileName.text = it.namaDosen
            tvDosenProfileNip.text = it.nipDosen
            tvDosenProfileEmail.text = it.emailDosen
        }
    }
}