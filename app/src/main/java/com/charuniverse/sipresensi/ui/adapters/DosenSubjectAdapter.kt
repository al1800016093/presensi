package com.charuniverse.sipresensi.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.item_subject_class.view.*

class DosenSubjectAdapter(
    private val viewModel: DosenViewModel,
    private val items: List<MataKuliah>
) : RecyclerView.Adapter<DosenSubjectAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_subject_class, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.itemView.let {
            it.tvSubjectClass.text = item.nama_matakuliah

            it.setOnClickListener { _ ->
                viewModel.selectCurrentSubject(position)
                viewModel.navigateToSelectClass()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
