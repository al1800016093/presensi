package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.ui.adapters.DosenSubjectAdapter
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.fragment_dosen_select_subject.*

class DosenSelectSubjectFragment : Fragment(R.layout.fragment_dosen_select_subject) {

    private lateinit var viewModel: DosenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)

        ivDosenSelectSubjectBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(DosenViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
        listenToViewModel()
    }

    private fun listenToViewModel() {
        viewModel.allSubject.observe(viewLifecycleOwner, {
            setupRecylerUI(it)
        })
    }

    private fun setupRecylerUI(models: List<MataKuliah>) {
        recyclerDosenSubject.layoutManager = LinearLayoutManager(requireContext())
        recyclerDosenSubject.adapter = DosenSubjectAdapter(viewModel, models)
    }

}