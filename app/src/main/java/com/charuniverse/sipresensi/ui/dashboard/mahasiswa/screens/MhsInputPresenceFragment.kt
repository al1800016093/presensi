package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import kotlinx.android.synthetic.main.fragment_mhs_input_presence.*

class MhsInputPresenceFragment : Fragment(R.layout.fragment_mhs_input_presence) {

    private lateinit var viewModel: MahasiswaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        onButtonClicked()

        ivMhsInputPresenceBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun onButtonClicked() {
        tvPresenceMhsPresent.setOnClickListener {
            it.isEnabled = false
            pbInputPresence.visibility = View.VISIBLE

            val presence = viewModel.createPresence()
            viewModel.addPresence(presence)
        }

        tvPresenceMhsSick.setOnClickListener {
            findNavController().navigate(R.id.action_mhsInputPresence_to_mhsUploadPermissionFragment)
        }
    }

}