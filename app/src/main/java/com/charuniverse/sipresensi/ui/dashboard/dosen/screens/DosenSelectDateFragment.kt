package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.adapters.DosenDateAdapter
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.fragment_dosen_select_date.*


class DosenSelectDateFragment : Fragment(R.layout.fragment_dosen_select_date) {

    private lateinit var viewModel: DosenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)

        ivDosenSelectDateBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(DosenViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
                it.getPresenceBySubjectClass()
            }
        listenToViewModel()
    }

    private fun listenToViewModel() {
        viewModel.currentSubject.observe(viewLifecycleOwner, {
            tvDosenSelectDateSubject.text = "${it.nama_matakuliah} (${viewModel.currentClass})"
        })
        viewModel.presenceTimestamps.observe(viewLifecycleOwner, {
            setupRecyclerUI(it)
        })
    }

    private fun setupRecyclerUI(items: List<String>) {
        recyclerDosenDate.layoutManager = GridLayoutManager(requireContext(), 2)
        recyclerDosenDate.adapter = DosenDateAdapter(viewModel, items)
    }

}