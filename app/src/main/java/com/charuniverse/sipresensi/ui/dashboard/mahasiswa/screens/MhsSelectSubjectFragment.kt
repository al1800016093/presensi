package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.ui.adapters.MhsSubjectAdapter
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import kotlinx.android.synthetic.main.fragment_mhs_select_subject.*

class MhsSelectSubjectFragment : Fragment(R.layout.fragment_mhs_select_subject) {

    private lateinit var viewModel: MahasiswaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        listenToViewModel()

        ivMhsSelectSubjectBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun listenToViewModel() {
        viewModel.subject.observe(viewLifecycleOwner, {
            setupRecyclerUI(it)
        })
    }

    private fun setupRecyclerUI(items: List<MataKuliah>) {
        recyclerSubject.let {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.adapter = MhsSubjectAdapter(viewModel, items)
        }
    }

}