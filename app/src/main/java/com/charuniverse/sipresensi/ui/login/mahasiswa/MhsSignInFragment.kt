package com.charuniverse.sipresensi.ui.login.mahasiswa

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.login.LoginViewModel
import com.charuniverse.sipresensi.utils.Constants
import kotlinx.android.synthetic.main.fragment_mhs_sign_in.*

class MhsSignInFragment : Fragment(R.layout.fragment_mhs_sign_in) {

    private lateinit var viewModel: LoginViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel(view)
        onButtonClicked()
    }

    private fun initViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
            .also {
                it.initializeViewModel(view, requireActivity())
                it.getDataMahasiswa()
            }
    }

    private fun onButtonClicked() {
        ivSignInMhsBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        cvSignInMhs.setOnClickListener {
            it.isEnabled = false
            processSignIn()
        }

        btnSignUpMhs.setOnClickListener {
            findNavController().navigate(R.id.action_signInMhsFragment_to_signUpMhsFragment)
        }
    }

    private fun processSignIn() {
        val username = etSignInMhsUsername.text.toString()
        val password = etSignInMhsPassword.text.toString()

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(requireContext(), Constants.INPUT_ERROR, Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.validateLoginMahasiswa(username, password)
        cvSignInMhs.isEnabled = true
    }

}