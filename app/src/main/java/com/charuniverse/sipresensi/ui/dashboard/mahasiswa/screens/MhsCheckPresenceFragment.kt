package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.adapters.MhsPresenceAdapter
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import kotlinx.android.synthetic.main.fragment_mhs_check_presence.*

class MhsCheckPresenceFragment : Fragment(R.layout.fragment_mhs_check_presence) {

    private lateinit var viewModel: MahasiswaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        listenToViewModel()

        ivMhsCheckPresenceBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
                it.getPresenceBySubjectClass()
            }
    }

    private fun listenToViewModel() {
        viewModel.currentSubject.observe(viewLifecycleOwner, {
            tvMhsCheckPresenceDosen.text = it.nama_dosen
            tvMhsCheckPresenceSubject.text = it.nama_matakuliah
        })

        viewModel.myPresence.observe(viewLifecycleOwner, {
            recyclerPresence.layoutManager = LinearLayoutManager(requireContext())
            recyclerPresence.adapter = MhsPresenceAdapter(it)
        })
    }

}