package com.charuniverse.sipresensi.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment(R.layout.fragment_login) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onButtonClicked()
    }

    private fun onButtonClicked() {
        cvLoginAsDosen.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signInDosenFragment)
        }

        cvLoginAsMahasiswa.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signInMhsFragment)
        }
    }

}