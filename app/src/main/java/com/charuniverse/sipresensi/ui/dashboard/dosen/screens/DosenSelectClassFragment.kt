package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.ui.adapters.DosenClassAdapter
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.fragment_dosen_select_class.*

class DosenSelectClassFragment : Fragment(R.layout.fragment_dosen_select_class) {

    private lateinit var viewModel: DosenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        listenToViewModel()

        ivDosenSelectClassBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(DosenViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun listenToViewModel() {
        viewModel.currentSubject.observe(viewLifecycleOwner, {
            setupRecyclerUI(it)
        })
    }

    private fun setupRecyclerUI(model: MataKuliah) {
        val sortedClass = model.kelas.sorted()
        recyclerDosenClass.layoutManager = LinearLayoutManager(requireContext())
        recyclerDosenClass.adapter = DosenClassAdapter(viewModel, sortedClass)
    }
}