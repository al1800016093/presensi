package com.charuniverse.sipresensi.ui.dashboard.dosen.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.Presensi
import com.charuniverse.sipresensi.ui.adapters.DosenPresenceAdapter
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.fragment_dosen_presence.*

class DosenPresenceFragment : Fragment(R.layout.fragment_dosen_presence) {

    private lateinit var viewModel: DosenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)

        ivDosenPresenceBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(DosenViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
                it.getCurrentTimestampPresence()
            }
        listenToViewModel()
    }

    private fun listenToViewModel() {
        viewModel.currentSubject.observe(viewLifecycleOwner, {
            tvDosenPresenceDosen.text = it.nama_dosen
            tvDosenPresenceSubject.text = it.nama_matakuliah
        })
        viewModel.currentDatePresence.observe(viewLifecycleOwner, {
            setupRecyclerUI(it)
        })
    }

    private fun setupRecyclerUI(models: List<Presensi>) {
        recyclerDosenPresence.layoutManager = LinearLayoutManager(requireContext())
        recyclerDosenPresence.adapter = DosenPresenceAdapter(models)
    }

}