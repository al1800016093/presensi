package com.charuniverse.sipresensi.ui.dashboard.dosen

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.helper.Firestore
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.data.models.Presensi
import com.charuniverse.sipresensi.utils.Constants
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class DosenViewModel : ViewModel() {

    companion object {
        const val TAG = "DosenViewModel"

        private const val NETWORK_ERROR = Constants.NETWORK_ERROR_MESSAGE
    }

    private lateinit var view: View
    private lateinit var navController: NavController

    private val _allPresence = MutableLiveData<List<Presensi>>()
    private val _currentPresence = MutableLiveData<List<Presensi>>()
    private val _currentDatePresence = MutableLiveData<List<Presensi>>()
    private val _presenceTimestamps = MutableLiveData<List<String>>()

    private val _allSubject = MutableLiveData<List<MataKuliah>>()
    private val _currentSubject = MutableLiveData<MataKuliah>()

    var currentClass: String = ""
    var currentDate: String = ""

    val presenceTimestamps: LiveData<List<String>>
        get() = _presenceTimestamps

    val currentDatePresence: LiveData<List<Presensi>>
        get() = _currentDatePresence

    val allSubject: LiveData<List<MataKuliah>>
        get() = _allSubject

    val currentSubject: LiveData<MataKuliah>
        get() = _currentSubject

    init {
        getAllSubject()
        getAllPresensi()
    }

    fun initializeViewModel(view: View, navController: NavController) {
        this.view = view
        this.navController = navController
    }

    private fun getAllSubject() = viewModelScope.launch {
        try {
            _allSubject.value = Firestore.getMatakuliah()
        } catch (e: Exception) {
            Log.e(TAG, "getAllSubject: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    private fun getAllPresensi() {
        FirebaseFirestore.getInstance().collection("Presensi")
            .orderBy("nama_matakuliah", Query.Direction.ASCENDING)
            .orderBy("kelas", Query.Direction.ASCENDING)
            .orderBy("tanggal", Query.Direction.ASCENDING)
            .addSnapshotListener { docs, error ->
                if (error != null) {
                    makeSnackbar(NETWORK_ERROR)
                }

                _allPresence.value = docs?.toObjects(Presensi::class.java)
            }
    }

    fun selectCurrentSubject(position: Int) {
        _currentSubject.value = _allSubject.value?.get(position)
    }

    fun getPresenceBySubjectClass() {
        val docs = mutableListOf<Presensi>()
        val mk = _currentSubject.value!!.nama_matakuliah

        _allPresence.value?.forEach {
            if (it.nama_matakuliah == mk && it.kelas == currentClass) {
                docs.add(it)
            }
        }

        _currentPresence.value = docs
        getAllPresenceTimestamp()
    }

    private fun getAllPresenceTimestamp() {
        val docs = mutableListOf<String>()
        var lastTimetstamp = ""

        _currentPresence.value?.forEach {
            if (lastTimetstamp != convertTimestamp(it.tanggal)) {
                lastTimetstamp = convertTimestamp(it.tanggal)
                docs.add(lastTimetstamp)
            }
        }

        _presenceTimestamps.value = docs
    }

    fun getCurrentTimestampPresence() {
        val docs = mutableListOf<Presensi>()

        _currentPresence.value?.forEach {
            if (currentDate == convertTimestamp(it.tanggal)) {
                docs.add(it)
            }
        }

        _currentDatePresence.value = docs
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertTimestamp(timestamp: Timestamp): String {
        val milliseconds = timestamp.seconds * 1000 + timestamp.nanoseconds / 1000000
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val netDate = Date(milliseconds)
        return sdf.format(netDate).toString()
    }

    fun navigateToSelectSubject() {
        navController.navigate(R.id.action_dosenDashboardFragment_to_dosenSelectSubjectFragment)
    }

    fun navigateToSelectClass() {
        navController.navigate(R.id.action_dosenSelectSubjectFragment_to_dosenSelectClassFragment)
    }

    fun navigateToSelectDate() {
        navController.navigate(R.id.action_dosenSelectClassFragment_to_dosenSelectDateFragment)
    }

    fun navigateToPresence() {
        navController.navigate(R.id.action_dosenSelectDateFragment_to_dosenPresenceFragment)
    }

    private fun makeSnackbar(message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }
}