package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import kotlinx.android.synthetic.main.fragment_mhs_dashboard.*

class MhsDashboardFragment : Fragment(R.layout.fragment_mhs_dashboard) {

    private lateinit var viewModel: MahasiswaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        onButtonClicked()
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun onButtonClicked() {
        llMhsCheckPresence.setOnClickListener {
            viewModel.let {
                it.selectedFragment = "check_presence"
                it.navigateToSelectSubjectFragment()
            }
        }

        llMhsInputPresence.setOnClickListener {
            viewModel.let {
                it.selectedFragment = "input_presence"
                it.navigateToSelectSubjectFragment()
            }
        }

        llMhsProfile.setOnClickListener {
            findNavController().navigate(R.id.action_mhsDashboardFragment_to_mhsProfileFragment)
        }
    }
}