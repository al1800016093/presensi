package com.charuniverse.sipresensi.ui.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.models.Presensi
import com.charuniverse.sipresensi.utils.Constants
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.item_dosen_prensence.view.*
import java.text.SimpleDateFormat
import java.util.*

class DosenPresenceAdapter(
    private val items: List<Presensi>
) : RecyclerView.Adapter<DosenPresenceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_dosen_prensence, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.let {
            it.tvDosenPresenceDate.text = convertTimestamp(item.tanggal)
            it.tvDosenPresenceStatus.text = item.status
            it.tvDosenPresenceName.text = item.nama_mahasiswa
            it.tvDosenPresenceNim.text = item.nim_mahasiswa

            when (item.status) {
                Constants.PRESENSI_PRESENT -> {
                    it.llDosenPresenceContainer.setBackgroundColor(Color.parseColor("#3AC048"))
                }

                Constants.PRESENSI_SICK -> {
                    it.llDosenPresenceContainer.setBackgroundColor(Color.parseColor("#ACB3BF"))
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertTimestamp(timestamp: Timestamp): String {
        val milliseconds = timestamp.seconds * 1000 + timestamp.nanoseconds / 1000000
        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val netDate = Date(milliseconds)
        return sdf.format(netDate).toString()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}