package com.charuniverse.sipresensi.ui.dashboard.mahasiswa

import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.data.helper.Firestore
import com.charuniverse.sipresensi.data.helper.Storage
import com.charuniverse.sipresensi.data.models.MataKuliah
import com.charuniverse.sipresensi.data.models.Presensi
import com.charuniverse.sipresensi.utils.AppPreferences
import com.charuniverse.sipresensi.utils.Constants
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_mhs_input_presence.view.*
import kotlinx.android.synthetic.main.fragment_mhs_upload_permission.view.*
import kotlinx.coroutines.launch

class MahasiswaViewModel : ViewModel() {

    companion object {
        private const val TAG = "MahasiswaViewModel"

        private const val SICK = Constants.PRESENSI_SICK
        private const val PRESENT = Constants.PRESENSI_PRESENT
        private const val NETWORK_ERROR = Constants.NETWORK_ERROR_MESSAGE

        private val mahasiswa = AppPreferences.getDataMahasiswa()
    }

    private lateinit var view: View
    private lateinit var navController: NavController

    private val _presence = MutableLiveData<List<Presensi>>()
    private val _currentSubjectPresence = MutableLiveData<List<Presensi>>()

    private val _subject = MutableLiveData<List<MataKuliah>>()
    private val _currentSubject = MutableLiveData<MataKuliah>()

    var selectedClass: String = ""
    var selectedFragment: String = ""

    val subject: LiveData<List<MataKuliah>>
        get() = _subject

    val currentSubject: LiveData<MataKuliah>
        get() = _currentSubject

    val myPresence: LiveData<List<Presensi>>
        get() = _currentSubjectPresence

    fun initializeViewModel(view: View, navController: NavController) {
        this.view = view
        this.navController = navController
    }

    init {
        getUserPresence()
        getSubject()
    }

    private fun getSubject() = viewModelScope.launch {
        try {
            _subject.value = _subject.value ?: Firestore.getMatakuliah()
        } catch (e: Exception) {
            Log.e(TAG, "getMatakuliah: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    fun setCurrentSubject(position: Int) {
        _currentSubject.value = _subject.value?.get(position)
    }

    private fun getUserPresence() {
        FirebaseFirestore.getInstance().collection("Presensi")
            .orderBy("nama_matakuliah", Query.Direction.ASCENDING)
            .orderBy("kelas", Query.Direction.ASCENDING)
            .orderBy("tanggal", Query.Direction.DESCENDING)
            .addSnapshotListener { docs, error ->
                if (error != null) {
                    makeSnackbar(NETWORK_ERROR)
                }
                _presence.value = docs?.toObjects(Presensi::class.java)
            }
    }

    fun getPresenceBySubjectClass() {
        val docs = mutableListOf<Presensi>()
        val subject = _currentSubject.value!!.nama_matakuliah

        _presence.value?.forEach {
            if (it.nama_matakuliah == subject && it.kelas == selectedClass) {
                docs.add(it)
            }
        }

        _currentSubjectPresence.value = docs
    }

    fun createPresence(): Presensi {
        val subject = _currentSubject.value?.nama_matakuliah!!
        return Presensi(
            subject, selectedClass, PRESENT
        )
    }

    fun addPresence(presensi: Presensi) = viewModelScope.launch {
        try {
            Firestore.addPresensi(presensi)
            navigateInputToDashboard()
            makeSnackbar("Absensi Berhasil")
        } catch (e: Exception) {
            view.let {
                it.tvPresenceMhsPresent.isEnabled = false
                it.pbInputPresence.visibility = View.GONE
            }
            Log.e(TAG, "addPresence: ${e.message}", e)
            makeSnackbar(Constants.NETWORK_ERROR_MESSAGE)
        }
    }

    fun addPresencePermission(
        imageUri: Uri, presensi: Presensi
    ) = viewModelScope.launch {
        try {
            val storageUrl =
                "presensi/${presensi.nama_matakuliah}/${presensi.kelas}/${presensi.tanggal}"
            val imageUrl = Storage.uploadImageToStorage(imageUri, storageUrl)

            presensi.let {
                it.link_foto = imageUrl
                it.status = SICK
                addPresence(it)
            }
            makeSnackbar("Upload Perizinan Berhasil")
            navigatePermissionToDashboard()

        } catch (e: Exception) {
            view.let {
                it.btnUploadPhoto.isEnabled = true
                it.btnChangePhoto.isEnabled = true
                it.ivPermissionPhoto.isEnabled = true
                it.pbMhsUploadPermission.visibility = View.GONE
            }
            Log.e(TAG, "addPresencePermission: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    fun navigateToSelectSubjectFragment() {
        navController.navigate(R.id.action_mhsDashboardFragment_to_mhsSelectSubjectFragment)
    }

    fun navigateToSelectClassFragment() {
        navController.navigate(R.id.action_mhsSelectSubjectFragment_to_mhsSelectClassFragment)
    }

    fun navigateToPresenceFragment() {
        when (selectedFragment) {
            "check_presence" -> {
                navController.navigate(R.id.action_mhsSelectClassFragment_to_mhsCheckPresenceFragment)
            }
            "input_presence" -> {
                navController.navigate(R.id.action_mhsSelectClassFragment_to_mhsInputPresenceFragment)
            }
        }
    }

    fun navigateInputToDashboard() {
        navController.navigate(R.id.action_mhsInputPresenceFragment_to_mhsDashboardFragment)
    }

    fun navigatePermissionToDashboard() {
        navController.navigate(R.id.action_mhsUploadPermissionFragment_to_mhsDashboardFragment)
    }

    private fun makeSnackbar(message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

}