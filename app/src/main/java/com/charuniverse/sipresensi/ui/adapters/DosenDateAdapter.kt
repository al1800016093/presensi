package com.charuniverse.sipresensi.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenViewModel
import kotlinx.android.synthetic.main.item_presence_date.view.*

class DosenDateAdapter(
    private val viewModel: DosenViewModel,
    private val items: List<String>
) : RecyclerView.Adapter<DosenDateAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_presence_date, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.let {
            it.tvDosenSubjectPresenceDate.text = item

            it.setOnClickListener {
                viewModel.currentDate = item
                viewModel.navigateToPresence()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}