package com.charuniverse.sipresensi.ui.dashboard.mahasiswa.screens

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.charuniverse.sipresensi.R
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MahasiswaViewModel
import com.charuniverse.sipresensi.utils.Constants
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_mhs_upload_permission.*

class MhsUploadPermissionFragment : Fragment(R.layout.fragment_mhs_upload_permission) {

    private lateinit var viewModel: MahasiswaViewModel
    private var selectedFile: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViewModel(view)
        onButtonClicked()

        ivMhsUploadPermissionBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun onButtonClicked() {
        ivPermissionPhoto.setOnClickListener {
            openGallery()
        }

        btnChangePhoto.setOnClickListener {
            openGallery()
        }

        btnUploadPhoto.setOnClickListener {
            btnUploadPhoto.isEnabled = false
            btnChangePhoto.isEnabled = false
            ivPermissionPhoto.isEnabled = false

            selectedFile?.let {
                pbMhsUploadPermission.visibility = View.VISIBLE

                val presence = viewModel.createPresence()
                viewModel.addPresencePermission(selectedFile!!, presence)
            } ?: Snackbar.make(it, "Anda belum memilih foto apapun", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun initializeViewModel(view: View) {
        viewModel = ViewModelProvider(requireActivity())
            .get(MahasiswaViewModel::class.java)
            .also {
                it.initializeViewModel(view, findNavController())
            }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_GET_CONTENT).also {
            it.type = "image/*"
            startActivityForResult(it, Constants.REQUEST_CODE_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_IMAGE) {
            data?.data?.let {
                ivPermissionPhoto.setImageURI(it)
                selectedFile = it
            }
        }
    }

}