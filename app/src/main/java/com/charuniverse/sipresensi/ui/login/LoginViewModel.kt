package com.charuniverse.sipresensi.ui.login

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.charuniverse.sipresensi.data.helper.Firestore
import com.charuniverse.sipresensi.data.models.Dosen
import com.charuniverse.sipresensi.data.models.Mahasiswa
import com.charuniverse.sipresensi.ui.dashboard.dosen.DosenDashboardActivity
import com.charuniverse.sipresensi.ui.dashboard.mahasiswa.MhsDashboardActivity
import com.charuniverse.sipresensi.utils.AppPreferences
import com.charuniverse.sipresensi.utils.Constants
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {

    companion object {
        private const val TAG = "LoginViewModel"

        private const val NETWORK_ERROR = Constants.NETWORK_ERROR_MESSAGE
        private const val LOGIN_DOSEN_ERROR = Constants.LOGIN_DOSEN_ERROR_MESSAGE
        private const val LOGIN_MAHASISWA_ERROR = Constants.LOGIN_MAHASISWA_ERROR_MESSAGE
    }

    private lateinit var view: View
    private lateinit var activity: Activity

    private val _dataMahasiswa = MutableLiveData<List<Mahasiswa>>()
    private val _dataDosen = MutableLiveData<List<Dosen>>()


    fun initializeViewModel(view: View, activity: Activity) {
        this.view = view
        this.activity = activity
    }

    fun getDataDosen() = viewModelScope.launch {
        try {
            _dataDosen.value = _dataDosen.value ?: Firestore.getDosen()
        } catch (e: Exception) {
            makeSnackbar(NETWORK_ERROR)
        }
    }

    fun validateLoginDosen(nama: String, nip: String) {
        _dataDosen.value?.forEach {
            if (it.nama == nama && it.nip == nip) {
                saveDataDosen(it)
                navigateToDosenDashboard()
            }
        } ?: getDataDosen()
        makeSnackbar(LOGIN_DOSEN_ERROR)
    }

    fun addDosen(dosen: Dosen) = viewModelScope.launch {
        try {
            Firestore.addDosen(dosen)
            saveDataDosen(dosen)
            navigateToDosenDashboard()
        } catch (e: Exception) {
            Log.e(TAG, "addDosen: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    fun getDataMahasiswa() = viewModelScope.launch {
        try {
            _dataMahasiswa.value = _dataMahasiswa.value ?: Firestore.getMahasiswa()
        } catch (e: Exception) {
            Log.e(TAG, "getDataMahasiswa: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    fun validateLoginMahasiswa(username: String, password: String) {
        _dataMahasiswa.value?.forEach {
            if (it.username == username && it.password == password) {
                saveDataMahasiswa(it)
                navigateToMhsDashboard()
            }
        } ?: getDataMahasiswa()
        makeSnackbar(LOGIN_MAHASISWA_ERROR)
    }

    fun addMahasiswa(mahasiswa: Mahasiswa) = viewModelScope.launch {
        try {
            Firestore.addMahasiswa(mahasiswa)
            saveDataMahasiswa(mahasiswa)
            navigateToMhsDashboard()
        } catch (e: Exception) {
            Log.e(TAG, "addMahasiswa: ${e.message}", e)
            makeSnackbar(NETWORK_ERROR)
        }
    }

    private fun navigateToDosenDashboard() {
        activity.startActivity(Intent(activity, DosenDashboardActivity::class.java))
        activity.finish()
    }

    private fun navigateToMhsDashboard() {
        activity.startActivity(Intent(activity, MhsDashboardActivity::class.java))
        activity.finish()
    }

    private fun saveDataMahasiswa(mahasiswa: Mahasiswa) {
        AppPreferences.saveDataMahasiswa(mahasiswa)
    }

    private fun saveDataDosen(dosen: Dosen) {
        AppPreferences.saveDataDosen(dosen)
    }

    private fun makeSnackbar(message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }
}